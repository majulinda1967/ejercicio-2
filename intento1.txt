Prólogo

La noche es uno de los temas que ha inspirado a los artistas a lo largo de la historia, y sigue siendo hasta el día 
de hoy fuente de inspiración para cientos de creaciones. Cada artista incluye este fenómeno desde su propia perspectiva 
le da su propio enfoque, aquello que este evento genera en él: armonía, amor, soledad, estupefacción, son solo algunos de 
los tópicos más comunes dentro de la poética nocturna. No obstante, ¿desde cuándo el Nocturno se establece como una temática propia?
El Nocturno, una forma artística que sobresalió inicialmente en el campo musical con artistas como John Field 
y Frédéric Chopin, más tarde tendría sus paralelos en los demás campos de las artes, entre ellos el literario. 
El interés en la noche y en la constitución de nocturnos se extendió rápidamente en el siglo XIX e inicios del XX, 
en corrientes tales como el Romanticismo y, posteriormente, el simbolismo. Su rasgo más característico fue la 
adopción de la sinestesia, de modo que el espectador no solo pudiera disfrutar una obra visualmente, sino que sus 
demás sentidos hiciesen parte de este proceso. Dice Guerrero Almagro (2017, p. 138):
“Desde el prisma literario, la sinestesia adquiere un papel fundamental en este siglo. Los poetas aspiran a crear 
composiciones musicales, elevadas, que transmitan una impresión sin interesarse en exceso por el contenido de los 
mismos. De ser una realidad fascinante, la noche deriva casi en un estado de ánimo”.
Para la literatura, la noche se convirtió en un tema de vital importancia y el Nocturno en su forma, pues en este 
cabía lo onírico, el misticismo, lo mágico y, especialmente, las pasiones amorosas. Muchos de los poemas Nocturnos 
buscan dar cuenta de la intimidad de los amantes desde una posición menos material, haciendo ver el acto amoroso 
como un momento sublime, con una aspiración de elevación. En este espacio onírico, el poeta mediante el sueño que 
conlleva la noche, puede refugiarse en la intimidad de la oscuridad y recurrir a figuras que se alejen de la 
materialidad para acercarse a lo intangible. De acuerdo con Litvak (2009, p.104): “la noche se convierte para 
muchos poetas en una temática emergida como expresión intangible de un estado de ánimo subjetivo, propiciado por 
la oscuridad” (como se citó en Guerrero Almagro, 2017, p.  137).
Es claro que la temática ha sido popularizada principalmente por poetas masculinos, de los cuales podemos destacar 
al colombiano José Asunción Silva, con su serie de nocturnos, entre los que es altamente recordado el “Nocturno III”, 
o al nicaragüense modernista Rubén Darío en su obra “Cantos de vida y esperanza”. El Nocturno no era solo un tema 
místico, hogar de lo mágico de la oscuridad, sino que había una clara intención de mostrar la percepción amorosa con 
respecto a la relación que tiene la noche con las mujeres. Si se tiene en cuenta el poema de Silva (2006) podemos distinguir 
que no solo se habla de la belleza de la noche, sino también de una figura femenina muda y pálida, con una sombra esbelta 
y ágil que lo acompaña en este momento de la intimidad de los amantes. Este es un campo que principalmente está habitado 
por poetas masculinos, reconocido y aclamado por los mismos, exaltando la conexión de los amantes, las ausencias románticas, 
la luna como la compañera de los solitarios. Debido a esto, me surge una inquietud: si se han tenido en cuenta las mismas 
características y temáticas para abarcar el Nocturno, ¿dónde quedan las poetas que han sabido ingeniar diferentes y novedosas 
perspectivas para la fascinante noche? Como bien se sabe, ha habido mujeres con un talento inmensurable a lo largo de la 
historia, destacando en todas las épocas por su maravilloso uso de las letras, pero muchas veces el reconocimiento de sus 
voces es pasado por alto, quedando en el olvido tanto ellas como sus creaciones. Es por esto que el objetivo de esta 
antología poética no es simplemente resaltar la belleza que esconde el Nocturno, sino, por un lado, destacar a aquellas 
mujeres con una gran producción literaria, que demuestran en sus poemas una gran habilidad en cuanto a la sutilidad de la 
escritura del Nocturno, y por otra parte, dar cuenta de cómo hablando acerca de un tema en común, la noche que las ampara 
les dice cosas tan diferentes, expresando sensaciones como la soledad, el amor, la tranquilidad e incluso el erotismo.
Nombres tales como Josefine de la Torre, Gloria Fuertes, Idea Vilariño, Carilda Oliver Labra, Meira Delmar, Alejandra 
Pizarnik y Cristina Peri Rossi son aquellos que se verán a lo largo de esta antología, poetas que han logrado transmitir 
el sentimiento sinestésico del Nocturno y crear diferentes atmósferas a lo largo de sus poemas. Esta antología busca ser un 
homenaje a estas mujeres nacidas en países de habla hispana durante el siglo xx, en un intento por mostrar cómo la diferencia 
entre geografías, el tiempo en el que vivieron y sus diferentes posturas ideológicas no son impedimento para demostrar 
cómo las une un mismo factor: la fascinación por la noche, la belleza del amor y el desamor dentro del Nocturno poético. 

hola